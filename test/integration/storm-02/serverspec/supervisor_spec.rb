#
# Copyright (c) 2015-2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe 'Storm Supervisor Daemon' do
  it 'is running' do
    expect(service('storm-supervisor')).to be_running
  end

  it 'is launched at boot' do
    expect(service('storm-supervisor')).to be_enabled
  end
end

describe 'Storm LogViewer Daemon' do
  it 'is running' do
    expect(service('storm-logviewer')).to be_running
  end

  it 'is launched at boot' do
    expect(service('storm-logviewer')).to be_enabled
  end

  it 'is listening on correct port' do
    expect(port(8000)).to be_listening
  end
end
