#
# Copyright (c) 2015-2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe 'Storm Nimbus Daemon' do
  it 'is running' do
    expect(service('storm-nimbus')).to be_running
  end

  it 'is launched at boot' do
    expect(service('storm-nimbus')).to be_enabled
  end

  it 'is listening on correct port' do
    expect(port(6627)).to be_listening
  end

  remote_conf = '/opt/storm/bin/storm remoteconfvalue'

  it 'has the correct server list' do
    res = `#{remote_conf} storm.zookeeper.servers`
    expect(res).to eq("storm.zookeeper.servers: [zookeeper-storm.kitchen]\n")
  end

  it 'has the correct local directory' do
    res = `#{remote_conf} storm.local.dir`
    expect(res).to eq("storm.local.dir: /var/opt/storm/lib\n")
  end

  it 'has the correct nimbus host' do
    res = `#{remote_conf} nimbus.host`
    expect(res).to eq("nimbus.host: storm-kitchen-01.kitchen\n")
  end
end

describe 'Storm UI Daemon' do
  it 'is running' do
    expect(service('storm-ui')).to be_running
  end

  it 'is launched at boot' do
    expect(service('storm-ui')).to be_enabled
  end

  it 'is listening on correct port' do
    expect(port(8080)).to be_listening
  end

  curl = 'http_proxy="" curl -sS -X GET'
  url = 'http://localhost:8080'

  it 'returns no error' do
    result = `#{curl} #{url}/api/v1/cluster/summary`
    exp = /\{"error":(.+)\}/
    error = exp.match(result)
    expect(error).to be_nil
  end

  it 'exhibits the correct Nimbus' do
    result = `#{curl} #{url}/api/v1/cluster/configuration`
    exp = /.*"nimbus.host":"storm-kitchen-01.kitchen".*/
    expect(result).to match(exp)
  end

  it 'exhibits the correct Zookeeper list' do
    result = `#{curl} #{url}/api/v1/cluster/configuration`
    exp = /.*"storm.zookeeper.servers":\["zookeeper-storm.kitchen"\].*/
    expect(result).to match(exp)
  end
end

describe 'Storm LogViewer Daemon' do
  it 'is running' do
    expect(service('storm-logviewer')).to be_running
  end

  it 'is launched at boot' do
    expect(service('storm-logviewer')).to be_enabled
  end

  it 'is listening on correct port' do
    expect(port(8000)).to be_listening
  end
end
