#
# Copyright (c) 2015-2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Log4j configuration for cluster

log_dir = node['storm-platform']['log_dir']

default['storm-platform']['log4j']['cluster']['config'] = {
  'configuration' => [{
    'monitorInterval' => '60',
    'properties' => [{
      'property' =>
      [
        {
          'name' => 'pattern',
          'content' => '%d{yyyy-MM-dd HH:mm:ss.SSS} %c{1.} [%p] %msg%n'
        },
        {
          'name' => 'patternMetris',
          'content' => '%d %-8r %m%n'
        }
      ]
    }],
    'appenders' => [{
      'RollingFile' =>
      [
        {
          'name' => 'A1',
          'fileName' => "#{log_dir}/${sys:logfile.name}",
          'filePattern' => "#{log_dir}/${sys:logfile.name}.%i",
          'PatternLayout' => [{
            'pattern' => ['${pattern}']
          }],
          'Policies' => [{
            'SizeBasedTriggeringPolicy' => [{
              'size' => '100 MB'
            }]
          }],
          'DefaultRolloverStrategy' => [{
            'max' => '9'
          }]
        },
        {
          'name' => 'ACCESS',
          'fileName' => "#{log_dir}/access.log",
          'filePattern' => "#{log_dir}/access.log.%i",
          'PatternLayout' => [{
            'pattern' => ['${pattern}']
          }],
          'Policies' => [{
            'SizeBasedTriggeringPolicy' => [{
              'size' => '100 MB'
            }]
          }],
          'DefaultRolloverStrategy' => [{
            'max' => '9'
          }]
        },
        {
          'name' => 'METRICS',
          'fileName' => "#{log_dir}/metrics.log",
          'filePattern' => "#{log_dir}/metrics.log.%i",
          'PatternLayout' => [{
            'pattern' => ['${patternMetris}']
          }],
          'Policies' => [{
            'SizeBasedTriggeringPolicy' => [{
              'size' => '2 MB'
            }]
          }],
          'DefaultRolloverStrategy' => [{
            'max' => '9'
          }]
        }
      ],
      'Syslog' => [{
        'name' => 'syslog',
        'format' => 'RFC5424',
        'host' => 'localhost',
        'port' => '514',
        'protocol' => 'UDP',
        'appName' => '[${sys:daemon.name}]',
        'mdcId' => 'mdc',
        'includeMDC' => 'true',
        'facility' => 'LOCAL5',
        'enterpriseNumber' => '18060',
        'newLine' => 'true',
        'exceptionPattern' => '%rEx{full}',
        'messageId' => '[${sys:user.name}:S0]',
        'id' => 'storm'
      }]
    }],
    'loggers' => [{
      'Logger' =>
      [
        {
          'name' => 'backtype.storm.security.auth.authorizer',
          'level' => 'info',
          'AppenderRef' => [{
            'ref' => 'ACCESS'
          }]
        },
        {
          'name' => 'backtype.storm.metric.LoggingMetricsConsumer',
          'level' => 'info',
          'AppenderRef' => [{
            'ref' => 'METRICS'
          }]
        }
      ],
      'root' => [{
        'level' => 'info',
        'appender-ref' => [{ 'ref' => 'A1' }, { 'ref' => 'syslog' }]
      }]
    }]
  }]
}
