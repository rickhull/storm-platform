#
# Copyright (c) 2015-2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Storm package
default['storm-platform']['version']         = '0.10.0'
default['storm-platform']['checksum']        =
  '066d1f5343333efd9187d7b850047cf9b3f63d885811e9fdd6e50f949b432f62'
default['storm-platform']['mirror']          =
  'http://apache.mirrors.ovh.net/ftp.apache.org/dist/storm'

# Storm installation
default['storm-platform']['user']            = 'storm'
default['storm-platform']['prefix_root']     = '/opt'
default['storm-platform']['prefix_home']     = '/opt'
default['storm-platform']['prefix_bin']      = '/opt/bin'
default['storm-platform']['log_dir']         = '/var/opt/storm/log'
default['storm-platform']['data_dir']        = '/var/opt/storm/lib'
default['storm-platform']['java']            = {
  'centos' => 'java-1.8.0-openjdk-headless'
}
default['storm-platform']['auto_restart']    = true

# Cluster configuration
default['storm-platform']['role']            = 'storm-platform'
default['storm-platform']['hosts']           = [] # Use search when empty
default['storm-platform']['size']            = 3

# Set Nimbus node by its id
default['storm-platform']['nimbus_id']       = 1 # Between 1 and cluster size
default['storm-platform']['slots_ports']     = [] # Default ports if empty

# Storm configuration
# You can use erb notation with chef variables
default['storm-platform']['config']          = {

  ## These MUST be filled in for a storm configuration
  'storm.zookeeper.servers' => [
    'localhost'
  ],
  'storm.local.dir' => default['storm-platform']['data_dir'],
  'nimbus.host' => 'localhost'

  # These may optionally be filled in:
  # List of custom serializations
  # 'topology.kryo.register' => [
  #   'org.mycompany.MyType',
  #   {'org.mycompany.MyType2' => 'org.mycompany.MyType2Serializer'}
  # ],
  # List of custom kryo decorators
  # 'topology.kryo.decorators' => [
  #   'org.mycompany.MyDecorator'
  # ],
  # Metrics Consumers
  # 'topology.metrics.consumer.register' => [
  #   {
  #     'class' => 'backtype.storm.metric.LoggingMetricsConsumer',
  #     'parallelism.hint' => 1
  #   },
  #   {
  #     'class' => 'org.mycompany.MyMetricsConsumer',
  #     'parallelism.hint' => 1,
  #     'argument' => [
  #       {'endpoint' => 'metrics-collector.mycompany.org'}
  #     ]
  #   }
  # ]
}
