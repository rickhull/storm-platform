#
# Copyright (c) 2015-2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Log4j configuration for worker

log_dir = node['storm-platform']['log_dir']

default['storm-platform']['log4j']['worker']['config'] = {
  'configuration' => [{
    'monitorInterval' => '60',
    'properties' => [{
      'property' =>
      [
        {
          'name' => 'pattern',
          'content' => '%d{yyyy-MM-dd HH:mm:ss.SSS} %c{1.} [%p] %msg%n'
        },
        {
          'name' => 'patternNoTime',
          'content' => '%msg%n'
        }
      ]
    }],
    'appenders' => [{
      'RollingFile' =>
      [
        {
          'name' => 'A1',
          'fileName' => "#{log_dir}/${sys:logfile.name}",
          'filePattern' => "#{log_dir}/${sys:logfile.name}.%i.gz",
          'PatternLayout' => [{ 'pattern' => ['${pattern}'] }],
          'Policies' => [
            { 'SizeBasedTriggeringPolicy' => [{ 'size' => '100 MB' }] }
          ],
          'DefaultRolloverStrategy' => [{ 'max' => '9' }]
        },
        {
          'name' => 'STDOUT',
          'fileName' => "#{log_dir}/${sys:logfile.name}.out",
          'filePattern' => "#{log_dir}/${sys:logfile.name}.out.%i.gz",
          'PatternLayout' => [{
            'pattern' => ['${patternNoTime}']
          }],
          'Policies' => [{
            'SizeBasedTriggeringPolicy' => [{ 'size' => '100 MB' }]
          }],
          'DefaultRolloverStrategy' => [{ 'max' => '4' }]
        },
        {
          'name' => 'STDERR',
          'fileName' => "#{log_dir}/${sys:logfile.name}.err",
          'filePattern' => "#{log_dir}/${sys:logfile.name}.err.%i.gz",
          'PatternLayout' => [{
            'pattern' => ['${patternNoTime}']
          }],
          'Policies' => [{
            'SizeBasedTriggeringPolicy' => [{
              'size' => '100 MB'
            }]
          }],
          'DefaultRolloverStrategy' => [{
            'max' => '4'
          }]
        }
      ],
      'Syslog' => [{
        'name' => 'syslog',
        'format' => 'RFC5424',
        'host' => 'localhost',
        'port' => '514',
        'protocol' => 'UDP',
        'appName' => '[${sys:storm.id}:${sys:worker.port}]',
        'mdcId' => 'mdc',
        'includeMDC' => 'true',
        'facility' => 'LOCAL5',
        'enterpriseNumber' => '18060',
        'newLine' => 'true',
        'exceptionPattern' => '%rEx{full}',
        'messageId' => '[${sys:user.name}:${sys:logging.sensitivity}]',
        'id' => 'storm'
      }]
    }],
    'loggers' => [{
      'root' =>
      [
        {
          'level' => 'info',
          'appender-ref' => [{ 'ref' => 'A1' }, { 'ref' => 'syslog' }]
        }
      ],
      'Logger' => [
        {
          'name' => 'STDERR',
          'level' => 'INFO',
          'appender-ref' => [{ 'ref' => 'STDERR' }, { 'ref' => 'syslog' }]
        },
        {
          'name' => 'STDOUT',
          'level' => 'INFO',
          'appender-ref' => [{ 'ref' => 'STDOUT' }, { 'ref' => 'syslog' }]
        }
      ]
    }]
  }]
}
